# Assignment 3
**Due Feb 6<sup>th</sup> before class**

## Task 1
Convert the following to rgb format:
* e.g #d6a6b9 = rgb(214,166,185)
d6 = d * 16^1 + 6 * 16^0
   = 13 * 16 + 6 * 1
   = 214

a6 = a * 16^1 +  6 * 16^0
   = 10 * 16 + 6 * 1
   = 166

b9 = b * 16^1 + 9 * 16^0
   = 11 * 16 + 9 * 1
   = 185



* #4ad5b2
* #9b0f28
* #be70a7
* #320b65
* #c53c37


## Task 2
Convert the following to hex format:
e.g: rgb(123,56,245) = #7b38f5
123 = 16 * 7 + 1 * 11 = 7b
56  = 16 * 3 + 1 * 8  = 38
245 = 16 * 15 + 1 * 5 = f5

* rgb(12,228,113)
* rgb(12,226,25)
* rgb(112,124,21)
* rgb(213,132,13)
* rgba(112,223,14,.4)


## Task 3
List atleast 4 css properties that use rgb or hex colors as part of
their values.

1.
2.
3.
4.
